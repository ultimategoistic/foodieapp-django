This is a Django Project.

Project Title:-
Foodie App

Description:-
Where users can register, login and add food item details like ItemName, ItemDescription, ItemPrice and link to the Item Image, can also view, edit and delete items.

Installation Instructions:-

    virtualenv project_name
    source project_name/bin/activate
    git clone https://gitlab.com/ultimategoistic/foodieapp-django.git
    cd foodieapp-django
    pip install -r requirements.txt
    python manage.py migrate
    python manage.py runserver

Functionalities:
1. Register
2. Login
3. Add Food Items
4. View details
5. Edit details
6. Delete items
7. Check Profile details
8. Search through items
9. Paginations
10. Logout
