from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.

class Item(models.Model):

    def __str__(self):
        return self.item_name
    
    user_name = models.ForeignKey(User,on_delete=models.CASCADE,default=1,)
    item_name = models.CharField(max_length=100, verbose_name='Item Name')
    item_desc = models.CharField(max_length=200, verbose_name='Item Description')
    item_price = models.IntegerField(verbose_name='Item Price')
    item_image = models.CharField(max_length=500, blank=True, verbose_name='Item Image Link', default='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8GV0f6prb-DPesR3lFwG2UsJh94yNz7ijRQ&usqp=CAU')
    
    def get_absolute_url(self):
        return reverse("food:detail", kwargs={"pk": self.pk})