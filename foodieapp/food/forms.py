'''Importing forms from Django'''
from django import forms
from .models import Item

class ItemForm(forms.ModelForm):
    '''class Description'''
    class Meta:
        '''class Description'''
        model = Item
        fields = ['item_name', 'item_desc', 'item_price', 'item_image']
