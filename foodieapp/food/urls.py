from django.urls import path
from . import views
from django.contrib.auth.decorators import login_required

'''Namespacing is used in Django to prevent conflicts between URL patterns with the 
same name in different applications. By assigning a namespace to each application's 
URLs, you can ensure that the reverse() function and the {% url %} template tag will
always return the correct URL, even if there are multiple URL patterns with the same 
name in different applications.'''
app_name = "food"
urlpatterns = [
    #/
    path('', login_required(views.IndexClassView.as_view()), name='index'),
    #/1
    path("<int:pk>/", login_required(views.FoodDetail.as_view()), name='detail'),
    # add items
    path('add', login_required(views.CreateItem.as_view()), name='create_item'),
    # edit items
    path("update/<int:pk>/", login_required(views.UpdateItem.as_view()), name='update_item'),
    #delete items
    path("delete/<int:item_id>/", login_required(views.delete_item), name='delete_item'),
]