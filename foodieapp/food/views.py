from django.shortcuts import render, redirect

# Create your views here.
from .models import Item
from .forms import ItemForm
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic import CreateView
from django.views.generic import UpdateView

'''def index(request):
    item_list = Item.objects.all()
    #a = loader.get_template("food/index.html") 
    context = {
        "item_list": item_list,
    }
    #return HttpResponse(a.render(context, request)) This response is got with loader library.
    return render(request, "food/index.html", context)'''


class IndexClassView(ListView):
    model = Item
    template_name = 'food/index.html'
    context_object_name = 'item_list'
    paginate_by = 5
    
    def get_queryset(self):
        # Get the search query from the GET request
        itemname = self.request.GET.get('itemname', '')  # 'item_name' is a common query parameter name (you can change it)

        if itemname:
            # Call your search function to filter the queryset
            item_list = self.model.objects.filter(item_name__icontains=itemname)
        else:
            # Return full queryset if no search query
            item_list = self.model.objects.all()

        return item_list

'''def detail(request, item_id):
    item = Item.objects.get(pk=item_id)
    context = {
        "item" : item,
    }
    return render(request, "food/detail.html", context)'''

class FoodDetail(DetailView):
    model = Item
    template_name = 'food/detail.html'
    
'''
def create_item(request):
    form = ItemForm(request.POST or None)
    
    if form.is_valid():
        item = form.save()
        return redirect(item.get_absolute_url())

    return render(request, "food/item-form.html", {"form":form})
    '''
    
class CreateItem(CreateView):
    model = Item
    fields = ['item_name', 'item_desc', 'item_price', 'item_image']
    template_name = 'food/item-form.html'
    def form_valid(self,form):
        form.instance.user_name = self.request.user
        if not form.cleaned_data['item_image']:
            form.instance.item_image = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8GV0f6prb-DPesR3lFwG2UsJh94yNz7ijRQ&usqp=CAU'
        return super().form_valid(form)

class UpdateItem(UpdateView):
    model = Item
    fields = ['item_name', 'item_desc', 'item_price', 'item_image']
    template_name = 'food/item-update-form.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['item'] = self.object
        return context
'''
def update_item(request,item_id):
    item = Item.objects.get(id=item_id)
    form = ItemForm(request.POST or None, instance=item)
        
    if form.is_valid():
        form.save()
        return redirect('food:index')
    return render(request, "food/item-form.html", {"form":form, "item":item})
'''

def delete_item(request,item_id):
    item = Item.objects.get(id=item_id)
    
    if request.method == 'POST':
        item.delete()
        return redirect('food:index')
    return render(request, "food/item-delete.html", { "item" : item })